(function() {
	"use strict";
	//console.log("SEAF fired!");
//console.log("javascript is linked");

//do not under any circumstance remove this line
$(document).foundation()




//Load Up Animations
var fade = document.querySelectorAll("#landing_logo");

function landingBuilder(){
  //console.log("from buildPg()");
  TweenMax.to(fade, 3, {opacity:"1"});
}
window.addEventListener("load", landingBuilder, false);





//Enter site through button
var landingEnterButton = document.querySelector(".landing_button");
var landingEnterPage = document.querySelector("#landing_page");
var loadingScreenPage = document.querySelector("#loading_screen");
var site = document.querySelector("#site_loaded");


							//This is the function after clicking "Become a Legend button"
	function enterSite(){
		console.log("From Landing Button, It was clicked!")
		//document.getElementById('landing_page').style.display = "none";
		TweenMax.to(landingEnterPage, 3, {opacity:"0"});
		TweenMax.to(landingEnterPage, 3, {display:"none"}); //So it gets rid of everything on top of the transition
		document.getElementById('landing_button_div').style.display = "none"; //Needed to be set as none, since you could still click once it was set to 0 opacity



								//Loading Animation, this adds the class so it starts when the button is clicked vs it running in the background.

		//var loadingBarStart = document.getElementById("loading_bar");
		document.getElementById("loading_bar").className += " animating";

							//This is the Animation for when the loading screen bar is done loading
		function hideLoadingScreen(){
			TweenMax.to(loadingScreenPage, 1.5, {opacity:"0"});
			TweenMax.to(loadingScreenPage, 1.5, {display:"none"});
			TweenMax.to(site, 1.5, {display:"block"});

		}
		setInterval(hideLoadingScreen, 6000); //This is the delay it sets to excute the fading of the loading screen

	}
landingEnterButton.addEventListener("click", enterSite, false);



})();
