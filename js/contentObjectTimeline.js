var timelineData = {
  // add info for the cars here

  1992: {
    year : "1992",

    title : " A LENGEND was born",

    description : [
      "Odell Cornelious Beckham Jr. was born on 5th November 1992 in Baton Rouge Louisiana, USA, to Heather Van Norman and Odell Beckham Sr. His parents also happened to be accomplished athletes back in their time along with his stepdad Derek Mills. He is eldest of the three Beckham kids; he has a younger brother Kordell, and a sister Jasmine.",
    ],

    images : [
    				"1998_1.jpg"
    			],

  		},

      2013: {
        year : "2013",

          title : "College Phenom",

        description : [
          "In 2013, Odell was awarded with the Paul Hornug award, which is given annually to the best player in the major college football league. He finally said goodbye to the 2013 season with thrilling 57 receptions and eight touchdowns and later, was selected to play for the New York Giants in the big leagues, the NFL.",

        ],

        images : [
        				"2013.png"
        			],
      },

        2016: {
    			year: "2016",

    			  title : "NFL Rookie",

          description: [
            "In 2016, he became the fastest player in NFL history to reach both 200 career receptions and 4,000 career receiving yards. In 2016, he recorded his first 100-reception season and reached the NFL playoffs for the first time in his career, after helping the Giants to an 11-5 season record. Beckham has been named to the Pro Bowl in each of his first three seasons in the NFL, and has been named a second-team All-Pro twice.",

    			],

          images : [
          				"2016.png"
          			],
  		},

      2017: {
  			year : "2017",

  			  title : "Set Back",

        description : [
          "Beckham was seriously injured as soon as he hit the ground after leaping for Manning’s high throw. He lay for several minutes writhing, hand over face, before he was helped to the cart. He continued to grimace, foot in the air, as the cart drove him to the X-ray room.",

  			],

        images : [
        				"2017.png"
        			],
  		},







  	};
