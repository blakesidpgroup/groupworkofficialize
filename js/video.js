(function() {
	"use strict";
//javascript for video player

var video, toggleButton, toggleButtonx;
var volume = document.getElementById("vol-control");


function init() {
	//console.log("From Init()");
	video = document.querySelector("#myVideo");
	//console.log(video.currentSrc) //This determines which codac the video is being played in.
	toggleButtonx = document.querySelector("#restartVideo");
	toggleButton = document.querySelector("#playVideo");
	toggleButton.addEventListener("click", togglePlay, false);
	toggleButtonx.addEventListener("click", restart, false);
}




function togglePlay(evt) { //to get the video to fire and play
	console.log("from togglePlay()");

	//condition for pause and play
	if(video.paused == true)
	{
		console.log(evt.target);
		video.play();
		evt.target.src = "images/pause.svg";

	}
	else
	{
		video.pause();
		evt.target.src = "images/play.svg";
	}
}

function restart() {
        video.currentTime = 0;
    }

function mutePlayer (evt) {
                    if (video.muted) {
                        video.muted = false;
                        evt.target.src = "images/volumeon.svg";
                    } else {
                        video.muted = true;
                        evt.target.src = "images/mute.svg";
                    }
}


volume.addEventListener("input", function(){
  video.volume = this.value / 100;
});//for volume bar

window.addEventListener ("load", init, false); //make sures the video loads before they can interact (Until the entire page loads they cant hit play)
 document.getElementById("volumeOn").addEventListener("click", mutePlayer, false);


})();
